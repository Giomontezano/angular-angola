import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';

  isOpen = true;
  copiado = false;

  navegador: boolean;
  usuarioAutenticado = false;


  changeIcon() {
    if (this.isOpen) {
      this.isOpen = !this.isOpen;
      document.getElementById('menuIcon').classList.remove('fa-chevron-circle-left');
      document.getElementById('menuIcon').classList.add('fa-chevron-circle-right');
      document.getElementById('topo').style.width = '100%';
      return true;
    } else {
      this.isOpen = !this.isOpen;
      document.getElementById('menuIcon').classList.remove('fa-chevron-circle-right');
      document.getElementById('menuIcon').classList.add('fa-chevron-circle-left');
      document.getElementById('topo').style.width = '84%';
      return false;
    }
  }
 

  copiar(text: any) {
    this.copiado = true;
    text.select();
    document.execCommand('Copy');
    setTimeout(() => this.copiado = false, 3000);
  }
}
